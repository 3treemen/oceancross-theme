<?php
/**
 * Oceancross-theme Theme functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package oceancross-theme
 */

add_action( 'wp_enqueue_scripts', 'envo_multipurpose_parent_theme_enqueue_styles' );

/**
 * Enqueue scripts and styles.
 */
function envo_multipurpose_parent_theme_enqueue_styles() {
	wp_enqueue_script( 'child' , get_stylesheet_directory_uri() .'/js/child.js' , array( 'jquery'));
	wp_enqueue_style( 'envo-multipurpose-style', get_template_directory_uri() . '/style.css' );
	wp_enqueue_style( 'oceancross-theme-style',
		get_stylesheet_directory_uri() . '/style.css',
		array( 'envo-multipurpose-style' )
	);
}

