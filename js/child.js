// jQuery( document ).ready(function() {
//     if(jQuery('#shipping_method_0_flat_rate4').length) {
//     console.log('it exists');
//       jQuery('#ship-to-different-address').hide();
//   }
//   else {
//     console.log('nope');
//   }
// });
jQuery(document).ready(function ($) {
  $('ul#shipping_method input').each(function () {
    var $this = $(this);
    console.log($this.val().indexOf('flat_rate:4'));
    if ($this.val().indexOf("flat_rate:4") != -1) {
      if ($this.prop('checked')) {
        hideShippingFields();
      }
      if ($this.attr('type') == 'hidden') {
        hideShippingFields();
      }
    }
  });


  $(document).on('change', 'ul#shipping_method input', function () {
    var $this = $(this);
    if ($this.val().indexOf("flat_rate:4") != -1) {
      hideShippingFields();
    } else {
      showShippingFields();
    }
  });

  function hideShippingFields() {
    var $col1 = $('.col-1');
    $('#ship-to-different-address-checkbox').prop('checked', false);
    $('.woocommerce-shipping-fields').closest('.col-2').hide();
    $('.woocommerce-additional-fields').appendTo($col1);
    $col1.css({
      marginLeft: "auto",
      marginRight: "auto"
    });
  }

  function showShippingFields() {
    $col2 = $('.woocommerce-shipping-fields').closest('.col-2');
    var $col1 = $('.col-1');
    $('.woocommerce-additional-fields').appendTo($col2);
    $col2.show();
    $col1.css({
      marginLeft: "",
      marginRight: ""
    });
  }
});